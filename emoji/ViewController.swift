//
//  ViewController.swift
//  emoji
//
//  Created by Priscilla Vanny Amelia on 29/03/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var labelOutlet: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }


    @IBAction func buttonHello(_ sender: UIButton) {
        labelOutlet.text = "🥰"
    }
    @IBAction func btExplode(_ sender: UIButton) {
        labelOutlet.text = "🥳"
    }
    @IBAction func btWave(_ sender: UIButton) {
        labelOutlet.text = "👋🏼"
    }
}

